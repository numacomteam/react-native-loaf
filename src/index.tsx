import { NativeModules, Platform } from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-loaf' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const Loaf = NativeModules.RNLoaf
  ? NativeModules.RNLoaf
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

export const SHORT = 'short';
export const AVERAGE = 'average';
export const LONG = 'long';
export const TOP = 'top';
export const BOTTOM = 'bottom';

export type LoafDuration = 'short' | 'average' | 'long';
export type LoafGravity = 'top' | 'bottom';

export function show(message: string, duration: LoafDuration): void {
  Loaf.show(message, duration);
}

export function showWithGravity(
  message: string,
  duration: LoafDuration,
  gravity: LoafGravity
): void {
  Loaf.showWithGravity(message, duration, gravity);
}
