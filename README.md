# react-native-loaf

React Native bindings for the Loaf Swift library

## Installation

```sh
npm install react-native-loaf
```

## Usage

```js
import { multiply } from "react-native-loaf";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
