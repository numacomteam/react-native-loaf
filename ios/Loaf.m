#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNLoaf, NSObject)

RCT_EXTERN_METHOD(show:(NSString *)message
                  withDuration:(NSString *)duration)
RCT_EXTERN_METHOD(showWithGravity:(NSString *)message
                  withDuration:(NSString *)duration
                  withGravity:(NSString *)gravity)

@end
