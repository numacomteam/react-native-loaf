import Loaf

@objc(RNLoaf)
class RNLoaf: NSObject, RCTBridgeModule {
  
  static func moduleName() -> String! {
    return "RNLoaf"
  }
  
  static func requiresMainQueueSetup() -> Bool {
    return true
  }

  private static func getRootView() -> UIViewController {
    return (UIApplication.shared.delegate?.window??.rootViewController)!
  }

  private func show(message: String, duration: String, location: String?) -> Void {
    let loc = { () -> Loaf.Location in
      switch location {
      case "top":
        return Loaf.Location.top
      case "bottom":
        fallthrough
      default:
        return Loaf.Location.bottom
      }
    }()

    let dur = { () -> Loaf.Duration in
      switch duration {
      case "short":
        return Loaf.Duration.short
      case "long":
        return Loaf.Duration.long
      case "average":
        fallthrough
      default:
        return Loaf.Duration.average
      }
    }()
    DispatchQueue.main.async {
      Loaf(message, location: loc, presentingDirection: .vertical, sender: Self.getRootView()).show(dur)
    }
  }

  @objc(show:withDuration:)
  func show(_ message: String, duration: String) -> Void {
    self.show(message: message, duration: duration, location: "bottom")
  }
  
  @objc(showWithGravity:withDuration:withGravity:)
  func showWithGravity(_ message: String, duration: String, gravity: String) -> Void {
    self.show(message: message, duration: duration, location: gravity)
  }
}
