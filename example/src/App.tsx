import * as React from 'react';

import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import { AVERAGE, show } from '@yadagency/react-native-loaf';

export default function App() {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => show('This is a loaf', AVERAGE)}>
        <Text>Show loaf</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
